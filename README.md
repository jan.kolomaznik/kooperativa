# ICTPro školení Python pro Kooperativa
## Den 1
### Část 1 (zaměřená na účastníky ovládající základní syntaxi)
- [x] Pracovní prostředí (0.5 h)
    1. Visual Studio Code
    2. Jupyter notebook a interpretovaný python
    3. Moduly a balíčky
    4. Instalace balíčků pomocí pip
- [x] Přehled základních vlastností jazyka (0.5 h)
    1. Proměnné a reference
    2. Standardní datové typy
    3. Řídící struktury
- [x] Základní organizace kódu (1 h )
    1. Psaní funkcí
    2. Předávání parametrů
    3. Lokální proměnné
    4. Dokumentace
- [ ] Generátory a iterátory (1 h)
    1. Sekvenční datové typy
    2. Generované sekvence
    3. Čtení textových souborů
- [x] Funkce a metody (2 h)
    1. Poziční a pojmenované parametry
    2. Zpracování argumentů
    3. Vnořené funkce a funkcionální prvky
    4. Globální, lokální a vázané proměnné

### Část 2 (základy zpracování tabulkových dat)
- [x] Knihovna Pandas – základy (3 h)
    1. DataFrame a Series
    2. Transformace tabulkových dat
    3. Filtrování řádků a sloupců
    4. Konverze datových typů
    5. Agregace dat
    6. Načtení dat z Excelu/csv a export dat do Excelu/csv

## Den 2
### Část 1 (jak používat python efektivněji)
- [x] Best practice (1.5 h)
    1. Používané konvence (dle PEP 8)
    2. Jak psát dobře strukturovaný kód
    3. Ošetření chyb
    4. Základy návrhových vzorů
- [x] Knihovna pathlib (0.5 h)
    1. Základní práce s cestami souborů
    2. Ověření existence souboru/složky
    3. Probíhání složek
- [x] Ladění a logování (2 h)
    1. Ladění pomocí výpisů
    2. Standardní logovací knihovna
    3. Debuggery
- [x] Přístup k SQL pomocí sqlalchemy (1 h)
    1. Vytvoření connection pro MS SQL server databázi
    2. Načtení výsledku sql dotazu z databáze
    3. Propojení s knihovnou Pandas
## Část 2 (pokročilejší koncepty „pro náročné“)
- [x] Objektově orientované programování – zaměřené na porozumění objektově napsaného
kódu (2.5 h)
    1. Instance a třídy
    2. Metody a atributy
    3. Speciální metody
    4. Deskriptory
- [x] Knihovna argparse (0.5 h)
    1. Základy předávání argumentů

## Den 3
### Část 1 (zpracování dat pro analýzy)
- [ ] Knihovna Pandas – pokročilí (5 h)
    1. Rychlé opakování Pandas základy
    2. Převod sloupců na řádky a naopak
    3. Práce se sloupci datového typu list
    4. Tvorba kontingenčních tabulek
    5. Spojování dat z více sloupců do jednoho sloupce datového typu list
    6. Spojování dat z více dataframe
    7. Spojování dat pod sebe
    8. Spojování dat vedle sebe
    9. Rychlá vizualizace dat přímo pomocí Pandas

### Část 2 (vizualizace dat z analýzy)
- [ ] Vizualizace grafů pomocí knihovny Plotly (3 h)
    1. Příklady základních obrázků:
        - Scatter plots
        - Line charts
        - Histogram
        - Waterfall
    2. Základní statistická analýza
    3. Rychlé srovnání s jinými knihovnami pro kreslení obrázků

## Další případná témata
- [ ] Testování
    1. Testování knihoven a aplikací
    2. Organizace testů
- [x] Lineární regrese
    1. Úvod do problematiky lineární regrese
    2. Příprava dat pro model lineární regrese knihovny sklearn
    3. Ověření přesnosti modelu, interpretace výsledků
- [ ] Knihovna xlwings pro práci s Excelem z pythonu
- [ ] Knihovna pysimplegui pro vytvoření jednoduchého grafického okénka