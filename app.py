import argparse

def add(a, b): 
    return a + b

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("a", help="First number", type=int)
    parser.add_argument("b", help="Second number", type=int)
    parser.add_argument('-v', help='foo help')
    args = parser.parse_args()
    if args.v:
        print(f"app = {args.a} + {args.b}")
    print(add(args.a, args.b))